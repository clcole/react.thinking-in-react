import React from 'react';
import './App.css';

function ProductRow(props) {
  const product = props.product;
  const name = product.stocked
    ? product.name
    : <span style={{color: "red"}}>{product.name}</span>;

  return (
    <tr>
      <td>{name}</td>
      <td>{product.price}</td>
      <td>{product.category}</td>
    </tr>
  );
}

function ProductTable(props) {
  const {products, filterText, inStockOnly} = props;
  
  const rows = products.reduce((accumulator, product)  => {
    if(!product.stocked && inStockOnly) {
      return accumulator;
    }    

    if(product.name.toLowerCase().indexOf(filterText.toLowerCase()) === -1) {
      return accumulator;
    }

    accumulator.push(<ProductRow key={product.name} product={product} />);
    return accumulator;
  }, []);

  // const rows = [];
  // products.forEach(product => {
  //   if (!product.stocked && inStockOnly) {
  //     return;
  //   }

  //   if (product.name.toLowerCase().indexOf(filterText.toLowerCase()) === -1) {
  //     return;
  //   }

  //   rows.push(<ProductRow key={product.name} product={product} />);
  // });

  return (
    <table>
      <thead>
        <tr>
          <th>Name</th>
          <th>Price</th>
          <th>Category</th>
        </tr>
      </thead>
      <tbody>
        {rows}
      </tbody>
    </table>
  );
}

function SearchBar(props) {
  const {
    filterText, 
    inStockOnly, 
    onFilterTextChange, 
    onInStockOnlyChange
  } = props;

  const handleFilterTextChange = (event) => onFilterTextChange(event.target.value);
  const handleInStockOnlyChange = (event) => onInStockOnlyChange(event.target.checked);

  return (
    <form>
      <input 
        type="text" 
        value={filterText}
        onChange={handleFilterTextChange}
        placeholder="Search..." />
      <br />
      <input 
        type="checkbox"
        checked={inStockOnly}
        onChange={handleInStockOnlyChange} />In Stock Only
    </form>
  );
}

class FilterableProductTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {filterText: "", inStockOnly: false};
    this.handleFilterTextChange = this.handleFilterTextChange.bind(this);
    this.handleInStockOnlyChange = this.handleInStockOnlyChange.bind(this);
  }

  handleFilterTextChange(filterText) {
    this.setState({filterText});
  }

  handleInStockOnlyChange(inStockOnly) {
    this.setState({inStockOnly});
  }

  render() {
    const products = this.props.products;
    const {filterText, inStockOnly} = this.state;

    return (
      <div>
        <SearchBar 
          filterText={filterText}
          inStockOnly={inStockOnly}
          onFilterTextChange={this.handleFilterTextChange}
          onInStockOnlyChange={this.handleInStockOnlyChange} />
        <ProductTable 
          products={products}
          filterText={filterText}
          inStockOnly={inStockOnly} />
      </div>
    );
  }
}

const PRODUCTS = [
  {category: 'Sporting Goods', price: '$49.99', stocked: true, name: 'Football'},
  {category: 'Sporting Goods', price: '$9.99', stocked: true, name: 'Baseball'},
  {category: 'Sporting Goods', price: '$29.99', stocked: false, name: 'Basketball'},
  {category: 'Electronics', price: '$99.99', stocked: true, name: 'iPod Touch'},
  {category: 'Electronics', price: '$399.99', stocked: false, name: 'iPhone 5'},
  {category: 'Electronics', price: '$199.99', stocked: true, name: 'Nexus 7'}
];

function App() {
  return (
    <div>
      <FilterableProductTable products={PRODUCTS} />
    </div>
  );
}

export default App;
